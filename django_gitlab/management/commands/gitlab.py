import subprocess

import pyperclip
import requests
from git import Repo
from gitlab import Gitlab, GitlabAuthenticationError, GitlabGetError
from django.core.management.base import BaseCommand, CommandError
from environ import Env
import os
from django.conf import settings
from urllib.parse import urlencode, quote_plus

env = Env()
env_file = os.path.join(settings.BASE_DIR, 'www/.env')
if os.path.isfile(env_file):
    env.read_env(env_file=env_file)


def get_username(hostname):
    process = subprocess.Popen(['ssh', '-T', 'git@' + hostname],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
    stdout, stderr = process.communicate()
    return stdout.decode('UTF-8').split('@', 1)[-1][:-2]


def get_hostname():
    import platform
    return platform.node()


class Command(BaseCommand):
    help = 'Sets up GitLab for local development.'

    def handle(self, *args, **options):
        self.stdout.write('Running GitLab Command')

        repo = Repo('.')
        split_url = repo.remotes[0].url.split('@', 1)[-1].split(':', 1)
        gitlab_hostname = split_url[0]
        origin = 'https://' + split_url[0]
        url = 'https://' + '/'.join(split_url)
        username = get_username(gitlab_hostname)
        project_name = split_url[1].rsplit('.', 1)[0]
        project_id = quote_plus(project_name)
        token = env('GITLAB_TOKEN', default='')
        gl = None
        try:
            gl = Gitlab(url=origin, private_token=token)
            gl.auth()
        except GitlabAuthenticationError:
            token = ''

        if not token:
            self.stdout.write('Please create a new Personal Access Token:')
            self.stdout.write('- ' + origin + '/profile/personal_access_tokens')
            pyperclip.copy(project_name + ' on ' + get_hostname())
            self.stdout.write('- Name (in your clipboard): ' + project_name + ' on ' + get_hostname())
            self.stdout.write('- Expires At: ')
            self.stdout.write('- API')
            self.stdout.write('** Press enter once the Personal Access Token is on your clipboard **')
            input()
            token = pyperclip.paste()
            self.stdout.write('Append this to the file www/.env: GITLAB_TOKEN=')
            with open('www/.env', 'a') as f:
                f.write(f'GITLAB_TOKEN={token}\n')
                env.read_env(env_file=env_file)
            token = env('GITLAB_TOKEN', default='')
            gl = Gitlab(url=origin, private_token=token)
            gl.auth()
            # exit(1)

        environment_scope = ''

        project = gl.projects.get(project_id)

        ### FEATURE FLAGS ###
        self.stdout.write('Configuring Feature Flags...')
        # for key in ['K8S_SECRET_' + x for x in ('UNLEASH_API_URL', 'UNLEASH_API_IID', )]:
        iid_key = 'K8S_SECRET_' + 'UNLEASH_API_IID'
        url_key = 'K8S_SECRET_' + 'UNLEASH_API_URL'
        try:
            project.variables.get(iid_key)
            self.stdout.write('- Configured Already!')
        except GitlabGetError:
            try:
                project.variables.get(url_key)
                self.stdout.write('Please verify Feature Flags: (look for Configure in top right corner)')
                self.stdout.write(f'- {origin}/{project_name}/-/feature_flags')
            except GitlabGetError:
                self.stdout.write('Please configure Feature Flags: (look for Configure in top right corner)')
                self.stdout.write(f'- {origin}/{project_name}/-/feature_flags')
                self.stdout.write('** Press enter once the API URL is on your clipboard **')
                input()
                unlease_url = pyperclip.paste()
                project.variables.create(dict(key=url_key, value=unlease_url))
            self.stdout.write('** Press enter once the Instance ID is on your clipboard **')
            input()
            unleash_iid = pyperclip.paste()
            project.variables.create(dict(key=iid_key, value=unleash_iid))
        self.stdout.write('All updates must flow through the User Interface post initial configuration.')
        self.stdout.write(f'- {origin}/{project_name}/-/feature_flags')
        self.stdout.write(f'- {origin}/{project_name}/-/settings/ci_cd#js-variables\n\n')

        ### ERROR TRACKING ###
        self.stdout.write('Configuring Error Tracking...')
        headers = {'PRIVATE-TOKEN': token}
        key = 'K8S_SECRET_' + 'SENTRY_DSN'
        try:
            project.variables.get(key)
            self.stdout.write('- Configured Already!')
        except GitlabGetError:
            url = f'{origin}/api/v4/projects/{project_id}/error_tracking/settings'
            print(url)
            error_tracking = requests.get(
                url,
                headers=headers,
            ).json()
            if 'message' in error_tracking:
                sentry_token_creator_url = 'https://sentry.io/settings/account/api/auth-tokens/new-token/'
                if error_tracking['message'] == '404 Error Tracking Setting Not Found':
                    self.stdout.write('Please configure Error Tracking:')
                    self.stdout.write(f'- {origin}/{project_name}/-/settings/operations')
                    self.stdout.write(
                        f'**************************************************\n'
                        f'*************** Sentry Quick Start ***************\n'
                        f'**************************************************\n'
                        f'1. Head to Sentry, login or create an account at: \n'
                        f'   - https://sentry.io/auth/login/                \n'
                        f'2. Create a new Project to send your errors:      \n'
                        f'   - Sentry > Projects > Create Project           \n'
                        f'3a. Provide GitLab the Sentry API URL:            \n'
                        f'   - https://sentry.io                            \n'
                        f'3b. Create a new API Token for GitLab to utilize: \n'
                        f'   - {sentry_token_creator_url}                   \n'
                        f'   - Scopes: event:read, project:read             \n'
                        f'3c. Provide GitLab the API Token from 3b:         \n'
                        f'   - {origin}/{project_name}/-/settings/operations\n'
                        f'3d. Select (in the dropdown) the Project from     \n'
                        f'    Step 2.                                       \n'
                        f'**************************************************\n'
                    )
                    self.stdout.write('** Press enter to continue **')
                    input()
                else:
                    self.stderr.write('Something when wrong when using at Error Tracking API: ' + error_tracking['message'])
                    exit(1)

            self.stdout.write(
                'In Sentry, head to the Project Settings via'
                'Sentry > Settings > Projects > (select project) > '
                'Client Keys DSN'
            )
            self.stdout.write(f'- https://sentry.io\n')
            self.stdout.write('** Press enter Sentry DSN is on your clipboard **')
            value = pyperclip.paste()
            input()
            project.variables.create(dict(key=key, value=value))
        self.stdout.write('All updates must flow through the User Interface post initial configuration.')
        self.stdout.write(f'- {origin}/{project_name}/-/error_tracking')
        self.stdout.write(f'- {origin}/{project_name}/-/settings/ci_cd#js-variables\n\n')

        # try:
        #     project.variables.get(key)
        # except GitlabGetError:
        #     self.stdout.write('Please enable Error Tracking: (look for Enable error tracking in middle of screen)')
        #     self.stdout.write(f'- {origin}/{project_name}/-/error_tracking')
        #     self.stdout.write('** Press enter Instance ID is on your clipboard **')
        #     value = pyperclip.paste()
        #     input()
        #     project.variables.create(dict(key=key, value=value))
        # self.stdout.write('All updates must flow through the User Interface post initial configuration.')
        # self.stdout.write(f'- {origin}/{project_name}/-/error_tracking\n\n')
            # self.stderr.write('')
        #             for k, v in updates.items():
        #                 setattr(variable, k, v)
        #             variable.save()
        #         except GitlabGetError:
        #             updates['key'] = 'K8S_SECRET_' + key
        #             project.variables.create(updates)
        #
        # print(url.rsplit('.', 1)[0] + '/-/settings/ci_cd#js-variables')
