from django.apps import AppConfig


class DjangoGitLabConfig(AppConfig):
    name = 'django_gitlab'
    verbose_name = 'Django GitLab'
