.. django-gitlab documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to django-braces's documentation!
=========================================

You can view the code of our project or fork it and add your own mixins (please, send them back to us), on `Github`_.

.. toctree::
    :maxdepth: 2

    Context Processors <context_processors>
    Middleware <middleware>

`View our Changelog <changelog.html>`_

`Want to contribute? <contributing.html>`_



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _Github: https://github.com/brack3t/django-braces